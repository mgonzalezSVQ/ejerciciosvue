/* 
 * Miguel Carlos González Bejarano
 * mgonzalez@grupoitalica.com
 */

// Vue.js
const vm = new Vue({
    el: 'main',
    data:{
        minimo: 5,
        busqueda: '',
        juegos:[
            {
                titulo: 'Battlefield 1',
                genero: 'FPS',
                puntuacion: 9
            },
            {
                titulo: 'Civilization VI',
                genero: 'Estrategia',
                puntuacion: 10
            },
            {
                titulo: 'Resident Evil 7',
                genero: 'Survival Horror',
                puntuacion: 7
            }
        ] 
    },

    computed: {
        mejoresJuegos(){
            return this.juegos.filter((juego) => juego.puntuacion >= this.minimo);
            /*if(juego.puntuacion >= this.minimo){
                return this.juegos.filter(juego);
            }*/
        },
        buscarJuego(){
            return this.juegos.filter((juego) => juego.titulo.includes(this.busqueda));
        }
    }
});